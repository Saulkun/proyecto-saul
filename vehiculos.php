<html>
<head>
</head>
<meta charset="utf-8">
<style>
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
</style>
<body>
<?php
    if(!isset($_POST["enviar"])){
?>  
<form action="precio_vehiculos_poo.php" method="POST">
    Vehículo: 
    <select name="vehiculo">
        <option></option>
        <option value="bicicleta">Bicicleta</option>
        <option value="moto">Moto</option>
        <option value="coche">Coche</option>
        <option value="camion">Camión</option>
    </select>
    <br>
    <br>
    Kilómetros recorridos: <input type="number" name="kilometros" id="kilometros" />
    <br>
    <br>
    Carga (sólo para camión): <input type="number" name="carga" id="carga" />
    <br>
    <br>
    <input type="submit" id="enviar" name="enviar" value="Enviar" />
</form>
<?php
    }
    else{
        include "vehiculo.class.php";

        if (empty($_POST["vehiculo"]) || empty($_POST["kilometros"])) {
            echo "Falta algún campo por rellenar.";
        }

        elseif ($_POST["vehiculo"] == "camion" && empty($_POST["carga"])){
            echo "No has introducido la carga del camión.";
        }

        else{

            if ($_POST['vehiculo'] == 'camion') {  
                $vehiculo = new Camion ($_POST['vehiculo'], $_POST['kilometros'], $_POST['carga']);
                $vehiculo->precioVehiculo();
            }

            else{
                $vehiculo = new Vehiculo ($_POST['vehiculo'], $_POST['kilometros']);
                $vehiculo->precioVehiculo();
            }
        }
    }  
?>
</body>
</html>
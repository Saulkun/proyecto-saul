<?php

class User {
	private $pdo;

	protected $userId;
	protected $dni;
	protected $password;
	protected $name;
	protected $lastName;
	protected $province;
	protected $age;
	protected $address;
	protected $phoneNumber;
	protected $role;
	protected $registerDate;
	protected $status;

	public function __construct() {
		$this->pdo = Database::connect();
	}

	public function listUsers() {
		$stm = $this->pdo->prepare("SELECT * FROM usuarios");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function getUser(string $userId) {
		$stm = $this->pdo->prepare("SELECT usuarios.id as user_id, 
			usuarios.nombre as nombre_usuario,
			usuarios.estado as estado_usuario, 
			apellidos, dni, edad, direccion,
			estado.estado, telefono,
			privilegios.nivel, usuarios.privilegios,
			provincias.nombre as nombre_provincia, 
			provincias.id as id_provincia 
			FROM usuarios INNER JOIN estado
			ON usuarios.estado = estado.id 
			INNER JOIN privilegios 
			ON usuarios.privilegios = privilegios.id 
			INNER JOIN provincias 
			ON provincias.id = usuarios.provincia 
			WHERE usuarios.id = ?");
		$stm->execute(array($userId));
		return $stm->fetch();
	}

	public function listRegists() {
		$stm = $this->pdo->prepare("SELECT * FROM usuarios 
									WHERE estado = 2 
									ORDER BY fecha_registro ASC");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function addUser(User $user) {
		$sql = "INSERT INTO usuarios 
					(dni, 
					password, 
					nombre, 
					apellidos, 
					provincia, 
					edad, 
					direccion, 
					telefono, 
					privilegios,  
					estado) 
				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		$this->pdo->prepare($sql)->execute(
				array(
					$user->dni,
					$user->password,
					$user->name,
					$user->lastName,
					$user->province,
					$user->age,
					$user->address,
					$user->phoneNumber,
					$user->role,
					$user->status
				)
			);
	}

	public function acceptRequest(int $userId) {
		$sql = "UPDATE usuarios SET estado = 1 WHERE id = ?";
		$this->pdo->prepare($sql)->execute(array($userId));
	}


	public function disableUser(string $userId) {
		$sql = "UPDATE usuarios SET estado = 3 WHERE id = ?";
		$this->pdo->prepare($sql)->execute(array($userId));
	}

	public function enableUser(string $userId) {
		$sql = "UPDATE usuarios SET estado = 1 WHERE id = ?";
		$this->pdo->prepare($sql)->execute(array($userId));
	}

	public function updateUser(User $user) {
		$sql = "UPDATE usuarios SET 
					edad = ?,
					direccion = ?,
					provincia = ?,
					telefono = ?
					WHERE id = ?";

		$this->pdo->prepare($sql)->execute(
				array(
					$user->age,
					$user->address,
					$user->province,
					$user->phoneNumber,
					$user->userId
				)
			);
	}

	public function searchUser(string $search) {
		$stm = $this->pdo->prepare("SELECT * FROM usuarios 
			WHERE nombre LIKE '%$search%'
			OR id LIKE '%$search%'");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function listProvinces() {
		$stm = $this->pdo->prepare("SELECT provincias.id as id_provincia,
		 provincias.nombre as nombre_provincia
		 FROM provincias");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function listRoles() {
		$stm = $this->pdo->prepare("SELECT * FROM privilegios");
		$stm->execute();
		return $stm->fetchAll();
	}

	//Getters y setters

	public function getUserId() {}

	public function setUserId(int $id) {
		$this->userId = $id;
	}

	public function getDni() {}

	public function setDni(string $dni) {
		$this->dni = $dni;
	}

	public function getName() {}

	public function setName(string $name) {
		$this->name = $name;
	}

	public function setPassword(string $password) {
		$this->password = $password;
	}

	public function getLastName() {}

	public function setLastName(string $lastName) {
		$this->lastName = $lastName;
	}

	public function getProvince() {}


	public function setProvince(string $province) {
		$this->province = $province;
	}

	public function getAge() {}

	public function setAge(int $age) {
		$this->age = $age;
	}

	public function getAddress() {}

	public function setAddress(string $address) {
		$this->address = $address;
	}

	public function getPhoneNumber() {}

	public function setPhoneNumber(int $phoneNumber) {
		$this->phoneNumber = $phoneNumber;
	}

	public function getRole() {}

	public function setRole(int $role) {
		$this->role = $role;
	}

	public function getStatus() {}

	public function setStatus(int $status) {
		$this->status = $status;
	}

	public function getRegisterDate() {}

	public function setRegisterDate(string $registerDate) {
		$this->registerDate = $registerDate;
	}
}
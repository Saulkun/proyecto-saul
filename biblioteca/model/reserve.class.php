<?php

class Reserve {
	private $pdo;

	protected $id;
	protected $userId;
	protected $bookId;
	protected $reserveDate;
	protected $limitDate;
	protected $startDate;
	protected $endDate;
	protected $backDate;
	protected $incident;
	protected $details;

	public function __construct() {
		$this->pdo = Database::connect();
	}

	public function listReserves() {
		$stm = $this->pdo->prepare("SELECT * FROM alquileres 
									INNER JOIN usuarios
									ON usuarios.id = alquileres.id
									INNER JOIN libros
									ON alquileres.codigo_libro = libros.codigo_libro 
									WHERE fecha_inicio IS NULL
									AND fecha_devolucion IS NULL
									ORDER BY fecha_limite DESC");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function listLoans() {
		$stm = $this->pdo->prepare("SELECT * FROM alquileres 
									INNER JOIN usuarios
									ON usuarios.id = alquileres.id
									INNER JOIN libros
									ON alquileres.codigo_libro = libros.codigo_libro 
									WHERE fecha_devolucion IS NULL
									AND fecha_inicio IS NOT NULL 
									ORDER BY fecha_final DESC");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function listAll() {
		$stm = $this->pdo->prepare("SELECT * FROM alquileres 
									INNER JOIN usuarios
									ON usuarios.id = alquileres.id
									INNER JOIN libros
									ON alquileres.codigo_libro = libros.codigo_libro 
									ORDER BY fecha_limite DESC");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function listUserReserves(string $userId) {
		$stm = $this->pdo->prepare("SELECT *, libros.titulo FROM alquileres
									INNER JOIN libros
									ON libros.codigo_libro = alquileres.codigo_libro
									WHERE alquileres.id = ?
									AND fecha_inicio IS NULL
									AND fecha_devolucion IS NULL 
									ORDER BY fecha_limite DESC");
		$stm->execute(array($userId));
		return $stm->fetchAll();
	}

	public function listUserLoans(string $userId) {
		$stm = $this->pdo->prepare("SELECT *, libros.titulo FROM alquileres
									INNER JOIN libros
									ON libros.codigo_libro = alquileres.codigo_libro
									WHERE alquileres.id = ?
									AND fecha_devolucion IS NULL
									AND fecha_inicio IS NOT NULL 
									ORDER BY fecha_limite DESC");
		$stm->execute(array($userId));
		return $stm->fetchAll();
	}

	public function addReserve(Reserve $reserve) {
		$sql = "INSERT INTO alquileres 
				(id,	
				codigo_libro,
				fecha_reserva,
				fecha_limite) 
				VALUES (?, ?, ?, ?)";

		$this->pdo->prepare($sql)->execute(
				array(
					$reserve->userId,
					$reserve->bookId,
					$reserve->reserveDate,
					$reserve->limitDate
				)
			);

		$sql = "UPDATE libros SET
				disponible = 'no'
				WHERE codigo_libro = ?";

		$this->pdo->prepare($sql)->execute(
				array(
					$_REQUEST['bookId']
				)
			);
	}

	public function startLoan(Reserve $reserve) {
		$sql = "UPDATE alquileres SET 
					fecha_inicio = ?,
					fecha_final = ?
					WHERE codigo_alquiler = ?";

		$this->pdo->prepare($sql)->execute(array($reserve->startDate,
								 $reserve->endDate, 
								 $reserve->id));
	}


	public function giveBack(Reserve $reserve) {
		$sql = "UPDATE alquileres SET 
					fecha_devolucion = ?
					WHERE codigo_alquiler = ?";
		$this->pdo->prepare($sql)->execute(array($reserve->backDate, $reserve->id));

		$sql = "UPDATE libros SET
				disponible = 'si'
				WHERE codigo_libro = ?";

		$this->pdo->prepare($sql)->execute(
				array(
					$_REQUEST['bookId']
				)
			);
	}

	public function searchReserve(string $search) {
		$stm = $this->pdo->prepare("SELECT *
			FROM alquileres 
			INNER JOIN usuarios
			ON usuarios.id = alquileres.id
			INNER JOIN libros
			ON alquileres.codigo_libro = libros.codigo_libro
			WHERE fecha_inicio IS NULL 
			AND nombre LIKE '%$search%' 
			OR alquileres.id LIKE '%$search%'
			OR titulo LIKE '%$search%'");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function searchLoan(string $search) {
		$stm = $this->pdo->prepare("SELECT *
			FROM alquileres 
			INNER JOIN usuarios
			ON usuarios.id = alquileres.id
			INNER JOIN libros
			ON alquileres.codigo_libro = libros.codigo_libro
			WHERE fecha_inicio IS NOT NULL
			AND nombre LIKE '%$search%' 
			OR alquileres.id LIKE '%$search%'
			OR titulo LIKE '%$search%'");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function listIncidents() {
		$stm = $this->pdo->prepare("SELECT id as id_incidencia, incidencia			
			FROM incidencias");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function allIncidents() {
		$stm = $this->pdo->prepare("SELECT *,
			alquileres.id as id_usuario, 
			incidencias.id as id_incidencia,
			incidencias.incidencia
			FROM alquileres
			INNER JOIN incidencias
			ON incidencias.id = alquileres.tipo_incidencia
			");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function addIncident(Reserve $reserve) {
		$sql = "UPDATE alquileres SET 
					tipo_incidencia = ?,
					descripcion_incidencia = ?
					WHERE codigo_alquiler = ?";
		$this->pdo->prepare($sql)->execute(array($reserve->incident, 
			$reserve->details, 
			$reserve->id));
	}
	public function searchIncident(string $search) {
		$stm = $this->pdo->prepare("SELECT *,
			alquileres.id as id_usuario, 
			incidencias.id as id_incidencia,
			incidencias.incidencia
			FROM alquileres
			INNER JOIN incidencias
			ON incidencias.id = alquileres.tipo_incidencia
			WHERE tipo_incidencia IS NOT NULL
			AND (alquileres.id LIKE '%$search%'
			OR codigo_libro LIKE '%$search%'
			OR codigo_alquiler LIKE '%$search%')");
		$stm->execute();
		return $stm->fetchAll();
	}

//Getters y setters

	public function getId() {
		return $this->id;
	}

	public function setId(int $id) {
		$this->id = $id;
	}

	public function getUserId() {
		return $this->userId;
	}

	public function setUserId(int $userId) {
		$this->userId = $userId;
	}

	public function getBookId() {
		return $this->bookId;
	}

	public function setBookId(int $bookId) {
		$this->bookId = $bookId;
	}

	public function getReserveDate() {
		return $this->reserveDate;
	}

	public function setReserveDate(string $reserveDate) {
		$this->reserveDate = $reserveDate;
	}

	public function getLimitDate() {
		return $this->limitDate;
	}

	public function setLimitDate(string $limitDate) {
		$this->limitDate = $limitDate;
	}

	public function getStartDate() {
		return $this->startDate;
	}

	public function setStartDate(string $startDate) {
		$this->startDate = $startDate;
	}

	public function getEndDate() {
		return $this->endDate;
	}

	public function setEndDate(string $endDate) {
		$this->endDate = $endDate;
	}

	public function getBackDate() {
		return $this->backDate;
	}

	public function setBackDate(string $backDate) {
		$this->backDate = $backDate;
	}

	public function getIncident() {
		return $this->incident;
	}

	public function setIncident(string $incident) {
		$this->incident = $incident;
	}

	public function getDetails() {
		return $this->details;
	}

	public function setDetails(string $details) {
		$this->details = $details;
	}
}
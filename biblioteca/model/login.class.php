<?php

class Login {
	private $pdo;

	private $username;
	private $password;

	function __construct() {
		$this->pdo = Database::connect();
	}

	public function validate(Login $login) {
		$stm = $this->pdo->prepare("SELECT id, password, nombre, privilegios, reservas, estado 
									FROM usuarios 
									WHERE dni = ?");
		$stm->execute(array($login->username));
		if ($stm->rowCount() == 1){
			return $stm->fetch();
		}
			echo "Usuario incorrecto.";
	}

	public function close() {
		session_start();
		session_unset();
		session_destroy();
	}

	public function getUsername() {
		return $this->username;
	}

	public function setUsername(string $username) {
		$this->username = $username;
	}

	public function getPassword() {
		return $this->password;
	}

	public function setPassword(string $password) {
		$this->password = $password;
	}
}
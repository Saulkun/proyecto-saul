<?php

class Admin {
	private $pdo;

	protected $id;
	protected $genre;
	protected $author;
	protected $editorial;	

	public function __construct() {
		$this->pdo = Database::connect();
	}

	public function listGenres() {
	$stm = $this->pdo->prepare("SELECT *
			FROM generos
			ORDER BY categoria ASC");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function addGenre(Admin $admin) {
		$sql = "INSERT INTO generos (categoria) VALUES (?)";
		$this->pdo->prepare($sql)->execute(
				array($admin->genre));
	}

	public function updateGenre(Admin $admin) {
		$sql = "UPDATE generos 
				SET	categoria = ? 
				WHERE id = ?";

		$this->pdo->prepare($sql)->execute(
				array($admin->genre, $admin->id));
	}

	public function listAuthors() {
		$stm = $this->pdo->prepare("SELECT *
			FROM autores
			ORDER BY nombre ASC");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function addAuthor(Admin $admin) {
		$sql = "INSERT INTO autores (nombre) VALUES (?)";
		$this->pdo->prepare($sql)->execute(
				array($admin->author));
	}

	public function updateAuthor(Admin $admin) {
		$sql = "UPDATE autores 
				SET	nombre = ? 
				WHERE id = ?";

		$this->pdo->prepare($sql)->execute(
				array($admin->author, $admin->id));
	}

	public function listEditorials() {
		$stm = $this->pdo->prepare("SELECT *
			FROM editoriales
			ORDER BY nombre ASC");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function addEditorial(Admin $admin) {
		$sql = "INSERT INTO editoriales (nombre) VALUES (?)";
		$this->pdo->prepare($sql)->execute(
				array($admin->editorial));
	}

	public function updateEditorial(Admin $admin) {
		$sql = "UPDATE editoriales 
				SET	nombre = ? 
				WHERE id = ?";

		$this->pdo->prepare($sql)->execute(
				array($admin->editorial, $admin->id));
	}

	public function getId() {}

	public function setId($id) {
		$this->id = $id;
	}

	public function getAuthor() {}

	public function setAuthor(string $author) {
		$this->author = $author;
	}

	public function getEditorial() {}

	public function setEditorial(string $editorial) {
		$this->editorial = $editorial;
	}

	public function getGenre() {}

	public function setGenre(string $genre) {
		$this->genre = $genre;
	}
}
<?php

class Database {
	public static function connect() {
		$pdo = new PDO('mysql:host=localhost;dbname=biblioteca;charset=utf8', 'root', '');
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $pdo;
	}
}
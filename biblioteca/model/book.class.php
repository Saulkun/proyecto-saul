<?php

class Book {
	private $pdo;

	protected $id;
	protected $cover;
	protected $title;
	protected $author;
	protected $publishDate;
	protected $managerId;
	protected $additionDate;
	protected $genre;
	protected $status;
	protected $available;
	protected $summary;
	protected $isbn;
	protected $editorial;
	protected $libraryId;
	protected $enabled;
	

	public function __construct() {
		$this->pdo = Database::connect();
	}

	public function listBooks() {
		$stm = $this->pdo->prepare("SELECT *, editoriales.nombre as nombre_editorial, 
			autores.nombre as nombre_autor
			FROM libros 
			INNER JOIN autores 
			ON libros.autor = autores.id
			INNER JOIN generos 
			ON libros.genero = generos.id
			INNER JOIN editoriales
			ON libros.editorial = editoriales.id 
			WHERE habilitado = 'si'
			ORDER BY titulo ASC");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function lastReleases() {
		$stm = $this->pdo->prepare("SELECT * FROM libros 
									WHERE habilitado = 'si'
									ORDER BY codigo_libro
									DESC LIMIT 6");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function getBook(string $bookId) {
		$stm = $this->pdo->prepare("SELECT *, generos.categoria, 
			editoriales.nombre as nombre_editorial, 
			autores.nombre as nombre_autor,
			autores.id as id_autor
			FROM libros INNER JOIN autores 
			ON libros.autor = autores.id 
			INNER JOIN generos 
			ON libros.genero = generos.id 
			INNER JOIN editoriales
			ON libros.editorial = editoriales.id 
			WHERE codigo_libro = ?");
		$stm->execute(array($bookId));
		return $stm->fetch();
	}

	public function addBook(Book $book) {
		$sql = "INSERT INTO libros 
					(titulo,	
					autor, 
					id_gestor, 
					fecha_publicacion,  
					genero, 
					estado_libro, 
					sinopsis,
					isbn,
					editorial,
					portada,
					biblioteca) 
					VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		$this->pdo->prepare($sql)->execute(
				array(
					$book->title,
					$book->author,
					$book->managerId,
					$book->publishDate,
					$book->genre,
					$book->status,
					$book->summary,
					$book->isbn,
					$book->editorial,
					$book->cover,
					$book->libraryId
				)
			);
	}

	public function disableBook(string $bookId) {
		$stm = $this->pdo->prepare("UPDATE libros SET habilitado = 'no', disponible = 'no' WHERE codigo_libro = ?");
		$stm->execute(array($bookId));
	}

	public function enableBook(string $bookId) {
		$stm = $this->pdo->prepare("UPDATE libros SET habilitado = 'si', disponible = 'si' WHERE codigo_libro = ?");
		$stm->execute(array($bookId));
	}

	public function updateBook(Book $book) {
		$sql = "UPDATE libros SET 
					titulo = ?,
					autor = ?,
					fecha_publicacion = ?,
					genero = ?,
					estado_libro = ?,
					sinopsis = ?,
					isbn = ?,
					editorial = ?
					WHERE codigo_libro = ?";

		$this->pdo->prepare($sql)->execute(
				array(
					$book->title,
					$book->author,
					$book->publishDate,
					$book->genre,
					$book->status,
					$book->summary,
					$book->isbn,
					$book->editorial,
					$book->id	
				)
			);
	}

	public function updateBookCover(Book $book) {
		$sql = "UPDATE libros SET 
					portada = ?,
					titulo = ?,
					autor = ?,
					fecha_publicacion = ?,
					genero = ?,
					estado_libro = ?,
					sinopsis = ?,
					isbn = ?,
					editorial = ?
					WHERE codigo_libro = ?";

		$this->pdo->prepare($sql)->execute(
				array(
					$book->cover,
					$book->title,
					$book->author,
					$book->publishDate,
					$book->genre,
					$book->status,
					$book->summary,
					$book->isbn,
					$book->editorial,
					$book->id	
				)
			);
	}

	public function searchBook(string $search) {
		$stm = $this->pdo->prepare("SELECT * , autores.nombre as nombre_autor, 
			generos.categoria, editoriales.nombre as nombre_editorial
			FROM libros
			INNER JOIN autores
			ON libros.autor = autores.id
			INNER JOIN generos
			ON libros.genero = generos.id
			INNER JOIN editoriales
			ON libros.editorial = editoriales.id
			WHERE habilitado = 'si'
			AND titulo LIKE '%$search%'
			OR autores.nombre LIKE '%$search%'
			OR categoria LIKE '%$search%'
			OR editoriales.nombre LIKE '%$search%'
			ORDER BY titulo");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function listDisabled() {
		$stm = $this->pdo->prepare("SELECT *, autores.nombre as nombre_autor,
			editoriales.nombre as nombre_editorial
			FROM libros INNER JOIN autores 
			ON libros.autor = autores.id
			INNER JOIN editoriales
			ON libros.editorial = editoriales.id
			INNER JOIN generos 
			ON libros.genero = generos.id
			WHERE habilitado = 'no'
			ORDER BY titulo ");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function listAuthorBooks(string $author) {
		$stm = $this->pdo->prepare("SELECT *, autores.nombre as nombre_autor,
			editoriales.nombre as nombre_editorial
			FROM libros 
			INNER JOIN autores
			ON libros.autor = autores.id
			INNER JOIN editoriales
			ON libros.editorial = editoriales.id
			INNER JOIN generos 
			ON libros.genero = generos.id  
			WHERE autor = ?");
		$stm->execute(array($author));
		return $stm->fetchAll();
	}

	public function listGenresBooks(string $genre) {
		$stm = $this->pdo->prepare("SELECT *, autores.nombre as nombre_autor,
			editoriales.nombre as nombre_editorial
			FROM libros 
			INNER JOIN autores 
			ON libros.autor = autores.id
			INNER JOIN editoriales
			ON libros.editorial = editoriales.id
			INNER JOIN generos 
			ON libros.genero = generos.id  
			WHERE genero = ?");
		$stm->execute(array($genre));
		return $stm->fetchAll();
	}

	public function listGenres(){
	$stm = $this->pdo->prepare("SELECT *
			FROM generos
			ORDER BY categoria ASC");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function listAuthors() {
		$stm = $this->pdo->prepare("SELECT *
			FROM autores
			ORDER BY nombre ASC");
		$stm->execute();
		return $stm->fetchAll();
	}

	public function listEditorials() {
		$stm = $this->pdo->prepare("SELECT *
			FROM editoriales
			ORDER BY nombre ASC");
		$stm->execute();
		return $stm->fetchAll();
	}

	//Getters y setters

	public function getId() {}

	public function setId(int $id) {
		$this->id = $id;
	}

	public function getCover() {}

	public function setCover(string $cover) {
		$this->cover = $cover;
	}

	public function getTitle() {}

	public function setTitle(string $title) {
		$this->title = $title;
	}

	public function getAuthor() {}

	public function setAuthor(string $author) {
		$this->author = $author;
	}

	public function getPublishDate() {}

	public function setPublishDate(string $publishDate) {
		$this->publishDate = $publishDate;
	}

	public function getManagerId() {}

	public function setManagerId(string $managerId) {
		$this->managerId = $managerId;
	}

	public function getAdditionDate() {}

	public function setAdditionDate(string $additionDate) {
		$this->additionDate = $additionDate;
	}

	public function getGenre() {}

	public function setGenre(string $genre) {
		$this->genre = $genre;
	}

	public function getStatus() {}

	public function setStatus(string $status) {
		$this->status = $status;
	}

	public function getAvailable() {}

	public function setAvailable(string $available) {
		$this->available = $available;
	}

	public function getSummary() {}

	public function setSummary(string $summary) {
		$this->summary = $summary;
	}

	public function getIsbn() {}

	public function setIsbn(string $isbn) {
		$this->isbn = $isbn;
	}

	public function getEditorial() {}

	public function setEditorial(string $editorial) {
		$this->editorial = $editorial;
	}

	public function getLibraryId() {}

	public function setLibraryId(string $libraryId) {
		$this->libraryId = $libraryId;
	}
}
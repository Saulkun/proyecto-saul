<?php
require_once 'settings/settings.php';
if (!isset($_SESSION['role']) || !isset($_SESSION['id'])){?>

	<div class="container pt-5">
		<h2 class="pt-5 text-center">Acceso denegado</h2>
	</div>

<?php
	exit();
}

elseif (!in_array($_SESSION['role'], Settings::ROLE_MANAGEMENT) &&
 		$_SESSION['id'] != $_REQUEST['userId']) {?>

	<div class="container pt-5">
		<h2 class="pt-5 text-center">Acceso denegado</h2>
	</div>

<?php
	exit();
}	

?>
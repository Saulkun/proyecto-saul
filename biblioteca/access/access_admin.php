<?php
require_once 'settings/settings.php';
if (!isset($_SESSION['role']) || $_SESSION['role'] != Settings::ROLE_ADMIN) {?>

	<div class="container pt-5">
		<h2 class="pt-5 text-center">Acceso denegado</h2>
	</div>

<?php
	exit();
}

?>
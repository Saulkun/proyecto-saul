<?php
require_once 'model/database.class.php';

if(!isset($_REQUEST['c'])) {
   require_once 'controller/book.controller.php';
   $controller = new BookController;
   $action = 'display';

   call_user_func(array($controller, $action)); 
}

else {
	// Obtiene el controlador a cargar
  	$controller = $_REQUEST['c'];
    $action = $_REQUEST['a'];

    // Instancia el controlador
    require_once "controller/$controller.controller.php";
    $controller = ucwords($controller) . 'Controller';
    $controller = new $controller;

    // Llama la accion
    call_user_func(array($controller, $action));
}

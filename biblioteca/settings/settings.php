<?php

class Settings {
	const ROLE_MANAGEMENT = [1, 2];
	const ROLE_ADMIN = 1;
	const ROLE_USER = 3;
	const LIMIT_RESERVE = 3;
	const LIMIT_PAGINATION = 10;
	const USER_ENABLED = 1;
	const USER_DISABLED = 3;
	const IMG_PATH = "assets/portadas/";
}

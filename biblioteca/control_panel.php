<?php
require_once "header.php";
require_once "access/access_manager.php";
?>
<div class="container pt-5" role="main">
	<h3 class="row pt-5">Panel de gestor</h3>
	<hr/>
	<div class="text-center">
		<div class="btn-group mr-3">
		  <button type="button" class="btn btn-primary dropdown-toggle btn-lg" 
		  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		  Libros
		  </button>
		  <div class="dropdown-menu">
		  	<a class="dropdown-item" href="index.php?c=book&a=new">Añadir libro</a>
		  	<a class="dropdown-item" href="index.php?c=admin&a=admin">Administrar datos</a>
		  	<a class="dropdown-item" href="index.php?c=book&a=indexDisabled">Libros deshabilitados</a>
		  </div>
		</div>

		<div class="btn-group mr-3">
		  <button type="button" class="btn btn-success dropdown-toggle btn-lg" 
		  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		  Usuarios
		  </button>
		  <div class="dropdown-menu">
		  	<a class="dropdown-item" href="index.php?c=user&a=indexRegists">Peticiones de registro</a>
		    <a class="dropdown-item" href="index.php?c=user&a=index">Buscar usuarios</a>
		  </div>
		</div>

		<div class="btn-group mr-3">
		  <button type="button" class="btn btn-info dropdown-toggle btn-lg" 
		  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		  Alquileres
		  </button>
		  <div class="dropdown-menu">
		  	<a class="dropdown-item" href="index.php?c=reserve&a=index">Ver reservas</a>
		  	<a class="dropdown-item" href="index.php?c=reserve&a=indexLoans">Ver alquileres</a>
		  	<a class="dropdown-item" href="index.php?c=reserve&a=indexAll">Histórico</a>
		  </div>
		</div>

		<div class="btn-group mr-3">
		  <button type="button" class="btn btn-warning dropdown-toggle btn-lg" 
		  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		  Incidencias
		  </button>
		  <div class="dropdown-menu">
		  	<a class="dropdown-item" href="index.php?c=reserve&a=indexIncidents">Ver incidencias</a>
		  </div>
		</div>

	</div>

<?php
//Controles admin

if (isset($_SESSION['id'])):
	if (($_SESSION['role'] == Settings::ROLE_ADMIN)):?>
		<h3 class="row pt-5">Panel de administrador</h3>
		<hr/>
		<div class="row pt-1" name="usuariosAdmin">
			<div class="col text-center"><a href="index.php?c=user&a=adminNew"><button class="btn-lg btn-success" role="button">Añadir usuario</button></a></div>
		</div>
<?php
	endif;
endif;
?>

</div>

<?php
require_once "footer.php";
?>
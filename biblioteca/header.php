<?php
	session_start();
	require_once 'settings/settings.php';
?>
<!doctype html>
<html lang="es">
<head>
	<title>Biblioteca Online</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body class="bg-light h-100" >
<nav class="p-3 navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="index.php">Biblioteca</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link font-weight-bold" href="index.php">Inicio<span class="sr-only"></span></a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle font-weight-bold" href="" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Catálogo</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="index.php?c=book&a=index">Libros</a>
              <a class="dropdown-item" href="index.php?c=book&a=authorsIndex">Autores</a>
              <a class="dropdown-item" href="index.php?c=book&a=genresIndex">Géneros</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link font-weight-bold" href="contacto.php">Contacto</a>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0 mr-5" method="post" action="index.php?c=book&a=search">
          	<input class="form-control mr-sm-2" type="text" placeholder="Buscar" aria-label="Search" name="search">
          	<button class="btn btn-outline-success my-2 my-sm-0" type="submit">
          		<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
					<path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
				</svg>
			</button>
        </form>

        <?php
        // Antes de iniciar sesión

        if (!isset($_SESSION['id'])) {
        ?>
			<a href="?c=login&a=newLogin" class=""><button type="button" class="btn btn-outline-light me-2">Iniciar Sesión</button></a>
			<a href="?c=user&a=new" class=""><button type="button" class="btn btn-warning">Registrarse</button></a>
			<?php
		}

		//Después de iniciar sesión
		else {
		?>
			<div class="dropdown">
				<a class="btn btn-outline-warning nav-link dropdown-toggle font-weight-bold me-2" href="#" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Bienvenido, <?php echo $_SESSION['name']?></a>
	            <div class="dropdown-menu" aria-labelledby="dropdown02">
	              <a class="dropdown-item" href="index.php?c=user&a=profile&userId=<?php echo $_SESSION['id'];?>">Perfil</a>
	              <a class="dropdown-item" href="index.php?c=reserve&a=myReserves&userId=<?php echo $_SESSION['id'];?>">Mis alquileres</a>

	              <?php 

	              if (in_array($_SESSION['role'], Settings::ROLE_MANAGEMENT)): ?>

	              <a class="dropdown-item" href="control_panel.php">Panel de control</a>

	              <?php
	              endif;
	              ?>

	              <div class="dropdown-divider"></div>
	              <a class="dropdown-item" href="index.php?c=login&a=end">Cerrar Sesión</a>
	         	</div>
	        </div>
		<?php
		}
		?>
</nav>
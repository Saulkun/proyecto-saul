<?php
require_once 'header.php';
?>

<div class="container mt-5">

	<h2 class="pt-5 text-center">Contacto</h2>

	<h4 class="pt-5">Puedes encontrarnos en:</h4>
	<h5 class="mt-3">Ornilla Doctor Kalea, 2, 48004 Bilbo, Bizkaia</h5>
	<h5 class="mt-3">O puedes mandarnos un correo a biblioteca@correo.com</h5>

	<div class="row mt-5">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d46500.50333283207!2d-2.856669903983393!3d43.24552265909335!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4e4fac87437727%3A0x364f27e82def0130!2sCIFP%20Txurdinaga%20LHII!5e0!3m2!1ses!2ses!4v1619992919234!5m2!1ses!2ses" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
	</div>



</div>


<?php
require_once 'footer.php';
?>
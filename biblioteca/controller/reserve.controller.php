<?php

require_once 'model/reserve.class.php';

class ReserveController {
	private $model;

	public function __construct() {
		$this->model = new Reserve;
	}

	public function index() {
		$reserves = $this->model->listReserves();
		require_once 'header.php';
		require_once 'access/access_manager.php';
		require_once 'view/reserve/reserve.view.php';
		require_once 'footer.php';
	}

	public function indexLoans() {
		$reserves = $this->model->listLoans();
		require_once 'header.php';
		require_once 'access/access_manager.php';
		require_once 'view/reserve/loan.view.php';
		require_once 'footer.php';
	}

	public function indexAll() {
		$reserves = $this->model->listAll();
		require_once 'header.php';
		require_once 'access/access_manager.php';
		require_once 'view/reserve/loan.view.php';
		require_once 'footer.php';
	}

	public function add() {
		require_once 'header.php';
		require_once 'access/access_user.php';

		$reserve = new Reserve();
		$date = new DateTime(date('d-m-Y'));
		$reserve->setReserveDate($date->format('d-m-Y'));
		$date->add(new DateInterval('P2D'));
		$reserve->setLimitDate($date->format('d-m-Y'));
		$userId = $_REQUEST['userId'];
		$reserve->setUserId($_REQUEST['userId']);
		$reserve->setBookId($_REQUEST['bookId']);
		
		$this->model->addReserve($reserve);
		header('Location: index.php?c=reserve&a=myReserves&userId=' . $userId);

		return $this;
	}

	public function start() {
		require_once 'header.php';
		require_once 'access/access_manager.php';
		$reserve = new Reserve();

		$date = new DateTime(date('d-m-Y'));
		$reserve->setStartDate($date->format('d-m-Y'));

		$date->add(new DateInterval('P7D'));
		$reserve->setEndDate($date->format('d-m-Y'));

		$reserve->setId($_REQUEST['reserveId']);

		$this->model->startLoan($reserve);
		header('Location: index.php?c=reserve&a=index');
	}

	public function end() {
		require_once 'header.php';
		require_once 'access/access_manager.php';

		$reserve = new Reserve();

		$date = new DateTime(date('d-m-Y'));
		$reserve->setBackDate($date->format('d-m-Y'));
		$reserve->setId($_REQUEST['reserveId']);

		$this->model->giveBack($reserve);
		header('Location: control_panel.php');
	}

	public function search() {
		$reserves = $this->model->searchReserve($_REQUEST['search']);
		require_once 'header.php';
		require_once 'access/access_manager.php';
        require_once 'view/reserve/reserve.view.php';
        require_once 'footer.php';
	}

	public function loanSearch() {
		$reserves = $this->model->searchLoan($_REQUEST['search']);
		require_once 'header.php';
		require_once 'access/access_manager.php';
        require_once 'view/reserve/loan.view.php';
        require_once 'footer.php';
	}

	public function myReserves() {
		$list = $this->model->listUserReserves($_REQUEST['userId']);
		$loan = $this->model->listUserLoans($_REQUEST['userId']);
		require_once 'header.php';
		require_once 'access/access_profile.php';
        require_once 'view/reserve/my_reserves.view.php';
        require_once 'footer.php';
	}

	public function newIncident() {
		$incidents = $this->model->listIncidents();
		require_once 'header.php';
		require_once 'access/access_manager.php';
        require_once 'view/reserve/newIncident.view.php';
        require_once 'footer.php';
	}

	public function sendIncident() {
		$reserve = new Reserve();

		$reserve->setId($_REQUEST['reserveId']);
		$reserve->setIncident($_REQUEST['incident']);
		$reserve->setDetails($_REQUEST['details']);

		$date = new DateTime(date('d-m-Y'));
		$reserve->setBackDate($date->format('d-m-Y'));

		$this->model->giveBack($reserve);
		$this->model->addIncident($reserve);

		require_once 'control_panel.php';
	}

	public function indexIncidents() {
			$incidents = $this->model->allIncidents();
			require_once 'header.php';
			require_once 'access/access_manager.php';
			require_once 'view/reserve/incidents.view.php';
			require_once 'footer.php';
	}

	public function incidentSearch() {
		$incidents = $this->model->searchIncident($_REQUEST['search']);
		require_once 'header.php';
		require_once 'access/access_manager.php';
        require_once 'view/reserve/incidents.view.php';
        require_once 'footer.php';
	}
}
<?php

require_once 'model/login.class.php';

class LoginController {
	private $model;

	public function __construct() {
		$this->model = new Login;
	}

	public function userLogin() {
		$login = new Login;

		$login->setUsername($_POST['username']);
		$login->setPassword($_POST['password']);

		$data = $this->model->validate($login);

		if ($data->estado == 3 || $data->estado == 2){
			$message = "Usuario deshabilitado.";
			header('Location: index.php?c=book&a=display');
			return $this;
		}
		
		if (password_verify($login->getPassword(), $data->password)) {
			session_start();
			$_SESSION['id'] = $data->id;
       		$_SESSION['name'] = $data->nombre;
       		$_SESSION['role'] = $data->privilegios;
       		$_SESSION['reserves'] = $data->reservas;
       		header('Location: index.php?c=book&a=display');
       		return $this;
		}

		$message = "La contraseña introducida es incorrecta."; 
		header('Location: index.php?c=login&a=newlogin');        
	}

	public function newLogin() {
		require_once 'view/login/login.view.php';
	}

	public function end() {
		$this->model->close();
		header('Location: index.php?c=book&a=display');
	}
}
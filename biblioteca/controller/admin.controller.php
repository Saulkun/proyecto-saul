<?php

require_once 'model/admin.class.php';

class AdminController {
	private $model;

	public function __construct() {
		$this->model = new Admin;
	}

	public function admin() {
		$genres = $this->model->listGenres();
		$authors = $this->model->listAuthors();
		$editorials = $this->model->listEditorials();

		require_once 'header.php';
		require_once 'access/access_manager.php';
		require_once 'view/admin/admin.view.php';
		require_once 'footer.php';
	}

	public function adminGenre() {
		require_once 'header.php';
		require_once 'access/access_manager.php';
		$admin = new Admin;

		$admin->setGenre($_REQUEST['genre']);

		switch ($_REQUEST['action']):

			case "add":
				$this->model->addGenre($admin);
				break;

			case "edit":
				$admin->setId($_REQUEST['selection']);
				$this->model->updateGenre($admin);
				break;

		endswitch;

		header('Location: index.php?c=admin&a=admin');
	}

	public function adminAuthor() {
		require_once 'header.php';
		require_once 'access/access_manager.php';
		$admin = new Admin;

		$admin->setAuthor($_REQUEST['author']);

		switch ($_REQUEST['action']):

			case "add":
				$this->model->addAuthor($admin);
				break;

			case "edit":
				$admin->setId($_REQUEST['selection']);
				$this->model->updateAuthor($admin);
				break;

		endswitch;

		header('Location: index.php?c=admin&a=admin');
	}

	public function adminEditorial() {
		require_once 'header.php';
		require_once 'access/access_manager.php';
		$admin = new Admin;

		$admin->setEditorial($_REQUEST['editorial']);

		switch ($_REQUEST['action']):

			case "add":
				$this->model->addEditorial($admin);
				break;

			case "edit":
				$admin->setId($_REQUEST['selection']);
				$this->model->updateEditorial($admin);
				break;

		endswitch;

		header('Location: index.php?c=admin&a=admin');
	}
}
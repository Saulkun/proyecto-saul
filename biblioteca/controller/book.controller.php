<?php

require_once 'model/book.class.php';

class BookController {
	private $model;

	public function __construct() {
		$this->model = new Book;
	}

	public function index() {
		$list = $this->model->listBooks();
		require_once 'header.php';
		require_once 'view/book/book.view.php';
		require_once 'footer.php';
	}

	public function display() {
		$list = $this->model->lastReleases();
		require_once 'header.php';
		require_once 'view/book/display.view.php';
		require_once 'footer.php';
	}

	public function crud() {
        $book = $this->model->getBook($_REQUEST['bookId']);
        $editorials = $this->model->listEditorials();
        $authors = $this->model->listAuthors();
        $genres = $this->model->listGenres();
        require_once 'header.php';
        require_once 'access/access_manager.php';
        require_once 'view/book/edit.view.php';
        require_once 'footer.php';
	}

	public function profile() {
        $book = $this->model->getBook($_REQUEST['bookId']);
        require_once 'header.php';
        require_once 'view/book/profile.view.php';
        require_once 'footer.php';
	}

	public function new() {
		$genres = $this->model->listGenres();
		$authors = $this->model->listAuthors();
		$editorials = $this->model->listEditorials();
		require_once 'header.php';
		require_once 'access/access_manager.php';
		require_once 'view/book/new.view.php';
		require_once 'footer.php';
	}

	public function add() {
		require_once 'header.php';
		require_once 'access/access_manager.php';

		$book = new Book;

		$book->setTitle($_REQUEST['title']);
		$book->setAuthor($_REQUEST['author']);
		$book->setManagerId($_REQUEST['managerId']);
		$book->setPublishDate($_REQUEST['publishDate']);
		$book->setGenre($_REQUEST['genre']);
		$book->setStatus($_REQUEST['status']);
		$book->setSummary($_REQUEST['summary']);
		$book->setIsbn($_REQUEST['isbn']);
		$book->setEditorial($_REQUEST['editorial']);
		$book->setCover($_FILES['cover']['name']);
		$book->setLibraryId("1");

		$this->model->addBook($book);
		header('Location: control_panel.php');
	}

	public function edit() {
		require_once 'header.php';
		require_once 'access/access_manager.php';

		$book = new Book;

		$book->setId($_REQUEST['id']);
		$book->setTitle($_REQUEST['title']);
		$book->setAuthor($_REQUEST['author']);
		$book->setPublishDate($_REQUEST['publishDate']);
		$book->setGenre($_REQUEST['genre']);
		$book->setStatus($_REQUEST['status']);
		$book->setSummary($_REQUEST['summary']);
		$book->setIsbn($_REQUEST['isbn']);
		$book->setEditorial($_REQUEST['editorial']);
		
		if (isset($_REQUEST['modCover'])) {
			$book->setCover($_FILES['cover']['name']);
			$this->model->updateBookCover($book);
			header('Location: index.php?c=book&a=profile&bookId=' . $_REQUEST['id']);
		}

		else{
			$this->model->updateBook($book);
			header('Location: index.php?c=book&a=profile&bookId=' . $_REQUEST['id']);
		}
	}

	public function disable() {
		require_once 'header.php';
		require_once 'access/access_manager.php';

		$this->model->disableBook($_REQUEST['bookId']);
		header('Location: index.php?c=book&a=profile&bookId=' . $_REQUEST['bookId']);
	}

	public function enable() {
		require_once 'header.php';
		require_once 'access/access_manager.php';

		$this->model->enableBook($_REQUEST['bookId']);
		header('Location: index.php?c=book&a=profile&bookId=' . $_REQUEST['bookId']);
	}

	public function search() {
		$list = $this->model->searchBook($_REQUEST['search']);
		require_once 'header.php';
		require_once 'view/book/book.view.php';
		require_once 'footer.php';
	}

	public function authorsIndex() {
		$list = $this->model->listAuthors();
		require_once 'header.php';
		require_once 'view/book/authors.view.php';
		require_once 'footer.php';
	}

	public function authorBooks() {
		$list = $this->model->listAuthorBooks($_REQUEST['author']);
		require_once 'header.php';
		require_once 'view/book/book.view.php';
		require_once 'footer.php';
	}

	public function genresIndex() {
		$list = $this->model->listGenres();
		require_once 'header.php';
		require_once 'view/book/genres.view.php';
		require_once 'footer.php';
	}

	public function genreBooks() {
		$list = $this->model->listGenresBooks($_REQUEST['genre']);
		require_once 'header.php';
		require_once 'view/book/book.view.php';
		require_once 'footer.php';
	}

	public function indexDisabled() {
		
		$list = $this->model->listDisabled();
		require_once 'header.php';
		require_once 'access/access_manager.php';
		require_once 'view/book/book.view.php';
		require_once 'footer.php';
	}
}
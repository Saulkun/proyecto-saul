<?php

require_once 'model/user.class.php';

class UserController {
	private $model;

	public function __construct() {
		$this->model = new User;
	}

	public function index() {
		$list = $this->model->listUsers();
		require_once 'header.php';
		require_once 'access/access_manager.php';
		require_once 'view/user/user.view.php';
		require_once 'footer.php';
	}

	public function indexRegists() {
		$list = $this->model->listRegists();
		require_once 'header.php';
		require_once 'access/access_manager.php';
		require_once 'view/user/regists.view.php';
		require_once 'footer.php';
	}

	public function crud() {
		$provinces = $this->model->listProvinces();
		$roles = $this->model->listRoles();
		$user = $this->model->getUser($_REQUEST['userId']);
        require_once 'header.php';
        require_once 'access/access_admin.php';
        require_once 'view/user/edit.view.php';
        require_once 'footer.php';
	}

	public function profile() {
        $user = $this->model->getUser($_REQUEST['userId']);
        require_once 'header.php';
        require_once 'access/access_profile.php';
        require_once 'view/user/profile.view.php';
        require_once 'footer.php';
	}

	public function new() {
		$provinces = $this->model->listProvinces();
		require_once 'view/user/register.view.php';
	}

	public function register() {
		$user = new User;

		$user->setDni($_REQUEST['dni']);
		$user->setPassword(password_hash($_POST['password'], PASSWORD_DEFAULT));
		$user->setName($_REQUEST['name']);
		$user->setLastName($_REQUEST['lastName']);
		$user->setProvince($_REQUEST['province']);
		$user->setAge($_REQUEST['age']);
		$user->setAddress($_REQUEST['address']);
		$user->setPhoneNumber($_REQUEST['phoneNumber']);
		$user->setRole(3);
		$user->setStatus(2);

		$this->model->addUser($user);
		header('Location: index.php?c=book&a=display');
	}

	public function adminNew() {
		$provinces = $this->model->listProvinces();
		$roles = $this->model->listRoles();
		require_once 'header.php';
		require_once 'access/access_admin.php';
        require_once 'view/user/new_admin.view.php';
        require_once 'footer.php';
	}

	public function adminAdd() {
		require_once 'header.php';
		require_once 'access/access_admin.php';

		$user = new User;

		$user->setDni($_REQUEST['dni']);
		$user->setPassword(password_hash($_POST['password'], PASSWORD_DEFAULT));
		$user->setName($_REQUEST['name']);
		$user->setLastName($_REQUEST['lastName']);
		$user->setProvince($_REQUEST['province']);
		$user->setAge($_REQUEST['age']);
		$user->setAddress($_REQUEST['address']);
		$user->setPhoneNumber($_REQUEST['phoneNumber']);
		$user->setRegisterDate($_REQUEST['date']);
		$user->setRole($_REQUEST['role']);
		$user->setStatus(1);

		$this->model->addUser($user);
		header('Location: control_panel.php');
	}

	public function accept() {
		require_once 'header.php';
		require_once 'access/access_manager.php';

		$this->model->acceptRequest($_REQUEST['userId']);
		header('Location: index.php?c=user&a=indexRegists');
	}

	public function edit() {
		require_once 'header.php';
		require_once 'access/access_admin.php';
		$user = new User;

		$user->setUserId($_REQUEST['userId']);
		$user->setAge($_REQUEST['age']);
		$user->setAddress($_REQUEST['address']);
		$user->setProvince($_REQUEST['province']);
		$user->setPhoneNumber($_REQUEST['phoneNumber']);

		$this->model->updateUser($user);
		header('Location: index.php?c=user&a=profile&userId=' . $_REQUEST['userId']);
	}

	public function disable() {
		require_once 'header.php';
		require_once 'access/access_admin.php';
		$this->model->disableUser($_REQUEST['userId']);
		header('Location: index.php?c=user&a=profile&userId=' . $_REQUEST['userId']);
	}

	public function enable() {
		require_once 'header.php';
		require_once 'access/access_admin.php';
		$this->model->enableUser($_REQUEST['userId']);
		header('Location: index.php?c=user&a=profile&userId=' . $_REQUEST['userId']);
	}

	public function search() {
		$users = $this->model->searchUser($_REQUEST['search']);
		require_once 'header.php';
		require_once 'access/access_manager.php';
        require_once 'view/user/user.view.php';
        require_once 'footer.php';
	}
}
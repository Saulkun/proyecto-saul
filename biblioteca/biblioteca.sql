-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-05-2021 a las 16:48:40
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `biblioteca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alquileres`
--

CREATE TABLE `alquileres` (
  `codigo_alquiler` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `codigo_libro` int(11) NOT NULL,
  `fecha_reserva` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_limite` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_inicio` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_final` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_devolucion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipo_incidencia` int(11) DEFAULT NULL,
  `descripcion_incidencia` text COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alquileres`
--

INSERT INTO `alquileres` (`codigo_alquiler`, `id`, `codigo_libro`, `fecha_reserva`, `fecha_limite`, `fecha_inicio`, `fecha_final`, `fecha_devolucion`, `tipo_incidencia`, `descripcion_incidencia`) VALUES
(34, 8, 2, '03-05-2021', '05-05-2021', '03-05-2021', '10-05-2021', '03-05-2021', 2, 'Cubierta dañada.'),
(40, 6, 8, '03-05-2021', '05-05-2021', '03-05-2021', '10-05-2021', '03-05-2021', 1, '2 días tarde.'),
(43, 6, 8, '03-05-2021', '05-05-2021', '03-05-2021', '10-05-2021', '03-05-2021', 1, '3 dias tarde');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autores`
--

CREATE TABLE `autores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `autores`
--

INSERT INTO `autores` (`id`, `nombre`) VALUES
(1, 'J.K.Rowling'),
(2, 'J.R.R Tolkien'),
(3, 'Stephen King'),
(4, 'Carlos Ruiz Zafón'),
(5, 'Jason Aaron'),
(6, 'Ken Follett'),
(7, 'Ildefonso Falcones'),
(8, 'Isaac Asimov');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `editoriales`
--

CREATE TABLE `editoriales` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `editoriales`
--

INSERT INTO `editoriales` (`id`, `nombre`) VALUES
(1, 'Salamandra'),
(2, 'Minotauro'),
(3, 'DeBolsillo'),
(4, 'Planeta'),
(5, 'Panini'),
(6, 'Plaza and Janes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `id` int(11) NOT NULL,
  `estado` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`id`, `estado`) VALUES
(1, 'activo'),
(2, 'pendiente'),
(3, 'desactivado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `generos`
--

CREATE TABLE `generos` (
  `id` int(11) NOT NULL,
  `categoria` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `generos`
--

INSERT INTO `generos` (`id`, `categoria`) VALUES
(1, 'Acción'),
(2, 'Aventura'),
(3, 'Biografía'),
(4, 'Ciencia ficción'),
(5, 'Comedia'),
(6, 'Documental'),
(7, 'Drama'),
(8, 'Fantasía'),
(9, 'Histórica'),
(10, 'Misterio'),
(11, 'Romance'),
(12, 'Terror'),
(13, 'Poesía'),
(14, 'Fábula'),
(15, 'Policíaca'),
(16, 'Novela'),
(17, 'Cómic'),
(18, 'Narrativa histórica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidencias`
--

CREATE TABLE `incidencias` (
  `id` int(11) NOT NULL,
  `incidencia` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `incidencias`
--

INSERT INTO `incidencias` (`id`, `incidencia`) VALUES
(1, 'Retraso'),
(2, 'Deterioro'),
(3, 'Pérdida');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libros`
--

CREATE TABLE `libros` (
  `codigo_libro` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `autor` int(11) NOT NULL,
  `sinopsis` text COLLATE utf8_spanish_ci NOT NULL,
  `portada` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `isbn` varchar(13) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_publicacion` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `editorial` int(11) NOT NULL,
  `biblioteca` int(11) NOT NULL,
  `id_gestor` int(11) NOT NULL,
  `fecha_adicion` timestamp NOT NULL DEFAULT current_timestamp(),
  `genero` int(11) DEFAULT NULL,
  `estado_libro` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `disponible` varchar(2) COLLATE utf8_spanish_ci DEFAULT 'si',
  `habilitado` varchar(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'si'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `libros`
--

INSERT INTO `libros` (`codigo_libro`, `titulo`, `autor`, `sinopsis`, `portada`, `isbn`, `fecha_publicacion`, `editorial`, `biblioteca`, `id_gestor`, `fecha_adicion`, `genero`, `estado_libro`, `disponible`, `habilitado`) VALUES
(2, 'Harry Potter y la piedra filosofal', 1, '\"Con las manos temblorosas, Harry le dio la vuelta al sobre y vio un sello de lacre púrpura con un escudo de armas: un león, un águila, un tejón y una serpiente, que rodeaban una gran letra H.\"\r\n\r\nHarry Potter nunca ha oído hablar de Hogwarts hasta que empiezan a caer cartas en el felpudo del número 4 de Privet Drive. Llevan la dirección escrita con tinta verde en un sobre de pergamino amarillento con un sello de lacre púrpura, y sus horripilantes tíos se apresuran a confiscarlas. Más tarde, el día que Harry cumple once años, Rubeus Hagrid, un hombre gigantesco cuyos ojos brillan como escarabajos negros, irrumpe con una noticia extraordinaria: Harry Potter es un mago, y le han concedido una plaza en el Colegio Hogwarts de Magia y Hechicería. ¡Está a punto de comenzar una aventura increíble!', 'Harry_Potter_y_la_Piedra_Filosofal_Portada_Español.jpg', '9788498382662', ' 01/06/2010', 1, 1, 6, '2021-05-02 19:47:34', 8, 'Perfecto', 'si', 'si'),
(3, 'Mr. Mercedes', 3, 'Justo antes del amanecer, en una decadente ciudad americana, cientos de parados esperan la apertura de la oficina de empleo para reclamar uno de los mil puestos de trabajo que se han anunciado. Han hecho cola durante toda la noche.\r\n\r\nDe pronto, invisible hasta que lo tienen prácticamente encima, un Mercedes surge de la fría niebla de la madrugada. Su conductor atropella y aplasta a todos los que encuentra a su alcance. Acto seguido, el coche da marcha atrás y vuelve a arremeter contra ellos. El asesino huye dejando atrás ocho muertos y quince heridos.\r\n\r\nMeses después, Bill Hodges, un policía jubilado que sigue obsesionado con este caso sin resolver, recibe una carta anónima de alguien que se declara culpable de la masacre.\r\n\r\nBrady Hartsfield vive con su madre alcohólica en la casa donde nació. Disfrutó tanto de aquella sensación de muerte debajo de los neumáticos del Mercedes que ahora quiere recuperarla.', 'mr_mercedes.jpg', '9788490627662', '12/11/2015', 3, 1, 8, '2021-05-03 09:20:59', 15, 'Perfecto', 'si', 'si'),
(4, 'Marina', 4, 'En la Barcelona de 1980 Óscar Drai sueña despierto, deslumbrado por los palacetes modernistas cercanos al internado en el que estudia. En una de sus escapadas conoce a Marina, una chica delicada de salud que comparte con Óscar la aventura de adentrarse en un enigma doloroso del pasado de la ciudad. Un misterioso personaje de la posguerra se propuso el mayor desafío imaginable, pero su ambición lo arrastró por sendas siniestras cuyas consecuencias debe pagar alguien todavía hoy.', 'marina.jpg', '9788408004349', '03/04/2012', 4, 1, 8, '2021-05-03 09:25:30', 16, 'Perfecto', 'si', 'si'),
(5, 'Thor: El trueno en las venas', 5, 'Después de que el Hijo de Odín perdiera su habilidad para levantar Mjolnir, alguien se hará en su lugar con el martillo... ¡y el Universo Marvel nunca volverá a ser el mismo! ¿Quién es la nueva Diosa del Trueno? Ni siquiera Odín lo sabe, pero ella podría ser la última esperanza de la Tierra frente a los Gigantes de Hielo.', 'd3edaefc9f87447daaac7fc8fe2cf7df.jpg', '9788413347578', '01/01/2021', 5, 1, 8, '2021-05-03 09:44:27', 17, 'Perfecto', 'no', 'si'),
(6, 'Una columna de fuego', 6, 'Una columna de fuego arranca cuando el joven Ned Willard regresa a su hogar en Kingsbridge por Navidad. Corre el año 1558, un año que trastocará la vida de Ned y que cambiará Europa para siempre.\r\n\r\nLas antiguas piedras de la catedral de Kingsbridge contemplan una ciudad dividida por el odio religioso. Los principios elevados chocan con la amistad, la lealtad y el amor, y provocan derramamientos de sangre. Ned se encuentra de pronto en el bando contrario al de la muchacha con quien anhela casarse, Margery Fitzgerald.\r\n\r\nCuando Isabel I llega al trono, toda Europa se vuelve en contra de Inglaterra. La joven monarca, astuta y decidida, organiza el primer servicio secreto del país para estar avisada ante cualquier indicio de intrigas homicidas, levantamientos o planes de invasión.', 'columna_1507151812002.jpg', '9788401018251', ' 12/09/2017', 6, 1, 8, '2021-05-03 09:52:49', 18, 'Perfecto', 'si', 'si'),
(7, 'La catedral del mar', 7, 'La ciudad de Barcelona se encuentra en su momento de mayor prosperidad; ha crecido hacia la Ribera, el humilde barrio de los pescadores, cuyos habitantes deciden construir, con el dinero de unos y el esfuerzo de otros, el mayor templo mariano jamás conocido: Santa María de la Mar.\r\n\r\nUna construcción que es paralela a la azarosa historia de Arnau, un siervo de la tierra que huye de los abusos de su señor feudal y se refugia en Barcelona, donde se convierte en ciudadano y, con ello, en hombre libre.\r\n\r\nEl joven Arnau trabaja como palafrenero, estibador, soldado y cambista. Una vida extenuante, siempre al amparo de la catedral de la mar, que le iba a llevar de la miseria del fugitivo a la nobleza y la riqueza. Pero con esta posición privilegiada también le llega la envidia de sus pares, que urden una sórdida conjura que pone su vida en manos de la Inquisición...', '9788499088044.jpg', '9788499088044', '01/12/2018', 3, 1, 8, '2021-05-03 09:55:36', 18, 'Perfecto', 'si', 'si'),
(8, 'Trilogía de la fundación', 8, 'El hombre se ha dispersado por toda la galaxia. La capital del Imperio es Trántor, nido de intrigas y corrupción. Gracias a su ciencia, fundada en el estudio matemático de los hechos históricos y el comportamiento de las masas, el psicohistoriador Hari Seldon prevé la caída del Imperio y el retorno a la barbarie durante varios milenios. A fin de reducir este período de barbarie a mil años, Seldon decide crear una Fundación en un extremo de la galaxia.\r\n\r\nEl poderío de la Fundación alcanza límites insospechados, su dominio se sostiene en la energía, la religión y el comercio. Sin embargo, la aparición del Mulo, un individuo dotado de poderes paranormales, desafía todas las previsiones. Conquistando planeta tras planeta, le gana terreno a la Fundación de manera vertiginosa. La salvación de la galaxia queda en manos de una Segunda Fundación totalmente secreta y cuyo emplazamiento es desconocido incluso para los dirigentes de la Primera.', '41lmBpTOuRL.jpg', '9788499083209', '02/07/2010', 3, 1, 8, '2021-05-03 10:03:51', 4, 'Perfecto', 'si', 'si');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilegios`
--

CREATE TABLE `privilegios` (
  `id` int(11) NOT NULL,
  `nivel` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `privilegios`
--

INSERT INTO `privilegios` (`id`, `nivel`) VALUES
(1, 'administrador'),
(2, 'gestor'),
(3, 'usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincias`
--

CREATE TABLE `provincias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `provincias`
--

INSERT INTO `provincias` (`id`, `nombre`) VALUES
(1, 'Bizkaia'),
(2, 'Araba'),
(3, 'Gipuzkoa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `dni` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `edad` int(11) NOT NULL,
  `provincia` int(11) NOT NULL,
  `ciudad` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` int(11) DEFAULT NULL,
  `privilegios` int(11) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `reservas` int(11) NOT NULL DEFAULT 0,
  `fecha_registro` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellidos`, `dni`, `password`, `edad`, `provincia`, `ciudad`, `direccion`, `telefono`, `privilegios`, `estado`, `reservas`, `fecha_registro`) VALUES
(6, 'admin', '-', 'admin', '$2y$10$gqQwTN.oFeXthw3FLiEV9eGdMNhQ8HwhJl89Q3zhdFKkmEAiAstsG', 0, 1, 'Bilbao', '-', 0, 1, 1, 0, '2021-05-02 16:43:52'),
(8, 'gestor', '-', 'gestor', '$2y$10$Jdl/2br2sQpHELBJXJhelOdjDyRw9.K19ce563r4/vx.IRR5AurIG', 0, 1, '', '-', 0, 2, 1, 0, '2021-05-02 16:46:01'),
(9, 'Saúl', 'Martín', '71348880M', '$2y$10$AFMuYoRdvuH0zQs4mSz2MuZJWkkws2pgx9h5Bgh.92KIWS0n86ApW', 30, 1, '', '48340, Amorebieta-Etxano', 666453213, 3, 1, 0, '2021-05-02 21:38:34'),
(10, 'usuario', ' ', 'usuario', '$2y$10$PgIEsCBan59WzH2xuCU6tODN6tKIlAk/Y/YOgfRzc4cm2wGQv7Mg2', 20, 1, '', '-', 675543321, 3, 2, 0, '2021-05-03 13:52:08');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alquileres`
--
ALTER TABLE `alquileres`
  ADD PRIMARY KEY (`codigo_alquiler`),
  ADD KEY `id` (`id`,`codigo_libro`,`tipo_incidencia`),
  ADD KEY `codigo_libro` (`codigo_libro`),
  ADD KEY `tipo_incidencia` (`tipo_incidencia`);

--
-- Indices de la tabla `autores`
--
ALTER TABLE `autores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `editoriales`
--
ALTER TABLE `editoriales`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `generos`
--
ALTER TABLE `generos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `incidencias`
--
ALTER TABLE `incidencias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `libros`
--
ALTER TABLE `libros`
  ADD PRIMARY KEY (`codigo_libro`),
  ADD KEY `autor` (`autor`,`editorial`,`id_gestor`,`genero`),
  ADD KEY `editorial` (`editorial`),
  ADD KEY `genero` (`genero`),
  ADD KEY `id_gestor` (`id_gestor`);

--
-- Indices de la tabla `privilegios`
--
ALTER TABLE `privilegios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `provincias`
--
ALTER TABLE `provincias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dni` (`dni`),
  ADD KEY `provincia` (`provincia`),
  ADD KEY `estado` (`estado`),
  ADD KEY `privilegios` (`privilegios`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alquileres`
--
ALTER TABLE `alquileres`
  MODIFY `codigo_alquiler` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `autores`
--
ALTER TABLE `autores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `editoriales`
--
ALTER TABLE `editoriales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `generos`
--
ALTER TABLE `generos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `incidencias`
--
ALTER TABLE `incidencias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `libros`
--
ALTER TABLE `libros`
  MODIFY `codigo_libro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `privilegios`
--
ALTER TABLE `privilegios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `provincias`
--
ALTER TABLE `provincias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alquileres`
--
ALTER TABLE `alquileres`
  ADD CONSTRAINT `alquileres_ibfk_1` FOREIGN KEY (`codigo_libro`) REFERENCES `libros` (`codigo_libro`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `alquileres_ibfk_2` FOREIGN KEY (`id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `alquileres_ibfk_3` FOREIGN KEY (`tipo_incidencia`) REFERENCES `incidencias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `libros`
--
ALTER TABLE `libros`
  ADD CONSTRAINT `libros_ibfk_1` FOREIGN KEY (`editorial`) REFERENCES `editoriales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `libros_ibfk_2` FOREIGN KEY (`genero`) REFERENCES `generos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `libros_ibfk_3` FOREIGN KEY (`id_gestor`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `libros_ibfk_4` FOREIGN KEY (`autor`) REFERENCES `autores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_2` FOREIGN KEY (`privilegios`) REFERENCES `privilegios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `usuarios_ibfk_3` FOREIGN KEY (`provincia`) REFERENCES `provincias` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `usuarios_ibfk_4` FOREIGN KEY (`estado`) REFERENCES `estado` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

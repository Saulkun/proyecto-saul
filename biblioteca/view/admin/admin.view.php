<div class="container pt-5 " role="main">
  <h3 class="mt-5">Géneros</h3>
  <hr>

    <form class="needs-validation" id="data" name="data" method="POST" action="?c=admin&a=adminGenre" >
        <select class="form-control" name="selection" />
          <option value="">Seleccione un género para editarlo</option>
            <?php
            foreach ($genres as $i):?>
          <option value="<?php echo $i->id;?>"><?php echo $i->categoria; endforeach;?></option>
        </select>

        <input type="text" class="form-control" name="genre" placeholder="Nuevo nombre">
               
      <div class="form-check form-check-inline mt-3">
        <input class="form-check-input" type="radio" name="action" id="inlineRadio1" value="add" checked>
        <label class="form-check-label" for="inlineRadio1">Añadir</label>
      </div>

      <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="action" id="inlineRadio2" value="edit">
        <label class="form-check-label" for="inlineRadio2">Editar</label>
      </div>

      <input type="submit" class="btn btn-primary ml-5" value="Enviar">
    </form>

  <h3 class="mt-5">Autores</h3>
  <hr>

  <form class="needs-validation" id="data" name="data" method="POST" action="?c=admin&a=adminAuthor" >
        <select class="form-control" name="selection" />
          <option value="">Seleccione un autor para editarlo</option>
            <?php
            foreach ($authors as $i):?>
          <option value="<?php echo $i->id;?>"><?php echo $i->nombre; endforeach;?></option>
        </select>

        <input type="text" class="form-control" name="author" placeholder="Nuevo nombre">
               
      <div class="form-check form-check-inline mt-3">
        <input class="form-check-input" type="radio" name="action" id="inlineRadio1" value="add" checked>
        <label class="form-check-label" for="inlineRadio1">Añadir</label>
      </div>

      <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="action" id="inlineRadio2" value="edit">
        <label class="form-check-label" for="inlineRadio2">Editar</label>
      </div>

      <input type="submit" class="btn btn-primary ml-5" value="Enviar">
  </form>

  <h3 class="mt-5">Editoriales</h3>
  <hr>

  <form class="needs-validation" id="data" name="data" method="POST" action="?c=admin&a=adminEditorial" >
        <select class="form-control" name="selection" />
          <option value="">Seleccione una editorial para editarla</option>
            <?php
            foreach ($editorials as $i):?>
          <option value="<?php echo $i->id;?>"><?php echo $i->nombre; endforeach;?></option>
        </select>

        <input type="text" class="form-control" name="editorial" placeholder="Nuevo nombre">
               
      <div class="form-check form-check-inline mt-3">
        <input class="form-check-input" type="radio" name="action" id="inlineRadio1" value="add" checked>
        <label class="form-check-label" for="inlineRadio1">Añadir</label>
      </div>

      <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="action" id="inlineRadio2" value="edit">
        <label class="form-check-label" for="inlineRadio2">Editar</label>
      </div>

      <input type="submit" class="btn btn-primary ml-5" value="Enviar">
  </form>

  <div class="text-center mt-5"><a href="control_panel.php"><button type="button" class="btn-lg btn-warning me-2">Volver</button></a></div>

</div>
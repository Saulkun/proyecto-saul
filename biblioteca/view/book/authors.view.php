<div class="container pt-5 h-100" role="main" id="index" name="index">
	<h1 class="pt-5 text-center" id="catalogo">Autores</h1>
	<?php
	foreach ($list as $i):?>
		<div class="row mx-auto mb-3 text-left">

			<div class="col-12 col-sm-4 col-md-7">
				<h5><a href="?c=book&a=authorBooks&author=<?php echo $i->id; ?>"><?php echo $i->nombre; ?></a></h5>
			</div>

		</div>

		<hr/>
	<?php
	endforeach;
	?>
</div>
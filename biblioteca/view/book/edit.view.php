<main class="form-signin pt-5" id="formRegister">
	<div class="container">
		<h2 class="row pt-5 text-center">Editar libro</h2>
		<form action="?c=book&a=edit" enctype="multipart/form-data" method="post" id="formulario" name="formulario">

			<div class="form-group">ID libro: <?php echo $book->codigo_libro; ?></div>

			<input type="hidden" id="id" name="id" value="<?php echo $book->codigo_libro; ?>" />

			<div class="text-center">
			<img class="mb-3" src="<?php echo Settings::IMG_PATH . $book->portada; ?>" width="180px" height="280px" />
			</div>

			<input type="checkbox" name="modCover"> Modificar portada

			<div class="form-group">Portada <input type="file" class="form-control" name="cover" /> </div>

			<div class="form-group">Título: <input class="form-control" type="text" name="title" value="<?php echo $book->titulo; ?>" /></div>

			<div class="form-group">Sinopsis: <textarea class="form-control" type="text" name="summary"><?php echo $book->sinopsis; ?></textarea></div>

			<div class="form-group">Autor:
				<select class="form-control" name="author" required/>
						<option value="<?php echo $book->autor;?>"><?php echo $book->nombre_autor;?></option>
						<?php
						foreach ($authors as $i):?>
						<option value="<?php echo $i->id;?>"><?php echo $i->nombre; endforeach?></option>
					</select>
			</div class="form-group">

			<div>Fecha de publicación: <input class="form-control" id="publishDate" name="publishDate" type="text" value="<?php echo $book->fecha_publicacion; ?>" /></div>

			<div class="form-group">Género:
				<select class="form-control" name="genre" required/>
						<option value="<?php echo $book->genero;?>"><?php echo $book->categoria;?></option>
						<?php
						foreach ($genres as $i):?>
						<option value="<?php echo $i->id;?>"><?php echo $i->categoria; endforeach?></option>
					</select>
			</div class="form-group">

			<div class="form-group">Editorial:
				<select class="form-control" name="editorial" required/>
						<option value="<?php echo $book->editorial;?>"><?php echo $book->nombre_editorial;?></option>
						<?php
						foreach ($editorials as $i):?>
						<option value="<?php echo $i->id;?>"><?php echo $i->nombre; endforeach?></option>
					</select>
			</div>

			<div class="form-group">Estado: <input class="form-control" name="status" type="text" value="<?php echo $book->estado_libro; ?>" /></div>

			<div class="form-group">ISBN:<input class="form-control" type="number" name="isbn" value="<?php echo $book->isbn; ?>" required/></div>

			<input class="btn btn-primary" type="submit" name="enviar" value="Editar" /></div>
		</form>
	</div>
</main>
<h1>Añadir libro</h1>

<main class="form-signin" id="formRegister">
		<form class="needs-validation" id="data" name="data" enctype="multipart/form-data" action="?c=book&a=add" method="POST" >
        	<h1 class="h3 mb-4 fw-normal text-center pt-5">Añadir un libro</h1>

			<div class="mb-3">
				<label for="title">Titulo</label><input class="form-control" type="text" name="title" required/>
		    </div>

		    <div class="mb-3">
		    	<label for="author">Autor</label>
				<select class="form-control" name="author" required/>
					<option value="">Selecciona un autor</option>
					<?php
					foreach ($authors as $i):?>
					<option value="<?php echo $i->id;?>"><?php echo $i->nombre; endforeach?></option>
				</select>
			</div>

			<div class="mb-3">
				<label for="summary">Sinopsis</label>
				<textarea class="form-control" name="summary" required/></textarea> 
		    </div>

			<div class="mb-3">
				<label for="isbn">ISBN</label>
				<input class="form-control" type="number" name="isbn" required/>
			</div>

			<div class="mb-3">
				<label for="publishDate">Fecha de publicación</label>
				<input class="form-control" type="text" name="publishDate" placeholder="dd/mm/aaaa" required/>
			</div>

			<div class="mb-3">
				<label for="genre">Género</label>
				<select class="form-control" name="genre" required/>
					<option value="">Selecciona un género</option>
					<?php
					foreach ($genres as $i):?>
					<option value="<?php echo $i->id;?>"><?php echo $i->categoria; endforeach?></option>
				</select>
			</div>

			<div class="mb-3">
				<label for="pages">Nº de páginas</label>
				<input class="form-control" type="number" name="pages" required/>
		    </div>

		    <div class="mb-3">
				<label for="editorial">Editorial</label>
				<select class="form-control" name="editorial" required/>
					<option value="">Selecciona una editorial</option>
					<?php
					foreach ($editorials as $i):?>
					<option value="<?php echo $i->id;?>"><?php echo $i->nombre; endforeach?></option>
				</select>
			</div>

			<div class="mb-3">
				<label for="status">Estado</label>
				<textarea class="form-control" name="status" /></textarea> 
		    </div>

		    <div class="mb-3">
				<label for="status">Portada</label>
				<input type="file" class="form-control" name="cover" required/> 
		    </div>

		    <input type="text" value="<?php echo $_SESSION['id']; ?>" name="managerId" hidden/>

			<button class="btn btn-primary btn-lg btn-block" type="submit" name="enviar" id="enviar">Enviar</button>

		</form>
	</main>
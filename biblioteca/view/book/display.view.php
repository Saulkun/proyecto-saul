<div class="container pt-5 text-center" role="main" id="display" name="display">
	<h1 class="pt-5" >Novedades</h1>
	<div class="row">
		<?php 
		foreach ($list as $i): ?>
			<div class="col mt-5 flex-column">

				<div class="text-center">
					<div class=""><a href="?c=book&a=profile&bookId=<?php echo $i->codigo_libro; ?>"><img src="<?php  echo Settings::IMG_PATH . $i->portada; ; ?>" width="270px" height="400px" /></a></div>

					<h5 class="mt-2"><a class="text-justify" href="?c=book&a=profile&bookId=<?php echo $i->codigo_libro; ?>"><?php echo $i->titulo; ?></a></h5>
				</div>

			</div>
		<?php 
		endforeach; ?>
	</div>
</div>


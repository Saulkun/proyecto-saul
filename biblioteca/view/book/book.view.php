
<div class="container pt-5 h-100" role="main" id="index" name="index">
	<h1 class="pt-5 text-center" id="catalogo">Catálogo</h1>
	<?php
	foreach ($list as $i):?>
		<div class="row mx-auto mb-3 text-left">
			<div class="col"><a href="?c=book&a=profile&bookId=<?php echo $i->codigo_libro; ?>"><img src="<?php echo Settings::IMG_PATH . $i->portada; ?>" width="130px" height="180px" /></a></div>

			<div class="align-self-center col-12 col-sm-4 col-md-7">
				<h5><a href="?c=book&a=profile&bookId=<?php echo $i->codigo_libro; ?>"><?php echo $i->titulo; ?></a></h5>
				<h5><?php echo $i->nombre_autor; ?></h5>
				<h6><?php echo $i->categoria; ?></h6>
				<h6><?php echo $i->nombre_editorial; ?></h6>
			</div>

		</div>

		<hr/>
	<?php
	endforeach;
	?>
</div>

<div class="container pt-5" role="main">

	<div class="row pt-5">
		<div class="col mr-2 align-self-center">
			<img class="mb-3" src="<?php echo Settings::IMG_PATH . $book->portada; ?>" width="300px" height="470px" />
		</div>

		<div class="col mr-5 align-self-center text-left">
			<h2 class="row mb-3"><?php echo $book->titulo ?></h2>

			<h4 class="row mb-3"><a href="index.php?c=book&a=authorBooks&author=<?php echo $book->id_autor; ?>"><?php echo $book->nombre_autor; ?></a></h4>

			<div class="row mb-3 text-justify"><?php echo $book->sinopsis ?></div>

			<hr/>

			<div class="row mb-1">ID libro: <?php echo $book->codigo_libro; ?></div>

			<div class="row mb-1">Género: <?php echo $book->categoria; ?></div>

			<div class="row mb-1">Editorial: <?php echo $book->nombre_editorial; ?></div>

			<div class="row mb-1">Fecha publicación: <?php echo $book->fecha_publicacion; ?></div>

			<div class="row mb-1">Estado: <?php echo $book->estado_libro; ?></div>

			<div class="row mb-1">Disponible: <?php echo $book->disponible; ?></div>
		</div>
	</div>

	<?php
	if (isset($_SESSION['id']) && $book->disponible == 'si' && $book->habilitado == 'si'):?>
		<div class="row pt-3">
			<div class="text-center"><a href="?c=reserve&a=add&bookId=<?php echo $book->codigo_libro;?>&userId=<?php echo $_SESSION['id'];?>"><button  type="button" class="btn-lg btn-primary me-2">Reservar</button></a></div>
		</div>
	<?php
	endif;?>

<?php
if (isset($_SESSION['id'])):
	if (in_array($_SESSION['role'], Settings::ROLE_MANAGEMENT)):?>
		<hr/>
		<div class="row pt-1">
			<div class="col text-center"><a href="?c=book&a=crud&bookId=<?php echo $book->codigo_libro; ?>"><button class="btn-lg btn-warning" role="button">Editar</button></a></div>

			<?php
			if ($book->habilitado == 'si'):?>
			<div class="col text-center"><a href="?c=book&a=disable&bookId=<?php echo $book->codigo_libro; ?>"><button class="btn-lg btn-danger" role="button">Deshabilitar</button></a></div>

			<?php
			endif;

			if ($book->habilitado == 'no'):?>
			<div class="col text-center"><a href="?c=book&a=enable&bookId=<?php echo $book->codigo_libro; ?>"><button class="btn-lg btn-success" role="button">Habilitar</button></a></div>

			<?php 
			endif;?>

		</div>
<?php
	endif;
endif;
?>
</div>
<main class="form-signin pt-5" id="formRegister">
		<form class="needs-validation" id="data" name="data" action="?c=user&a=adminAdd" method="POST" >
        	<h1 class="h3 mb-4 fw-normal text-center pt-5">Añadir usuario</h1>
        	<h5 class="mb-4 fw-normal text-center">¡Atención! El usuario se añadirá directamente al sistema</h5>

			<div class="row">

				<div class="col-md-6 mb-3">
					<label for="name">Nombre</label><input class="form-control" type="text" name="name" required/>
					<div class="invalid-feedback">
		                Introduce un nombre válido.
		            </div>
		        </div>

		        <div class="col-md-6 mb-3">
					<label for="lastName">Apellidos</label>
					<input class="form-control" type="text" name="lastName" required/>
					<div class="invalid-feedback">
		                Introduce unos apellidos válidos.
		            </div>
				</div>
			</div>

			<div class="mb-3">
				<label for="dni">DNI</label>
				<input class="form-control" type="text" name="dni" />
				<div class="invalid-feedback">
		            Introduce un dni válido.
		        </div>
			</div>

			<div class="mb-3">
				<label for="password">Contraseña</label>
				<input class="form-control" type="password" name="password" required/>
				<div class="invalid-feedback">
		            Introduce una contraseña válida.
		        </div>
			</div>

			<div class="mb-3">
				<label for="age">Edad</label>
				<input class="form-control" type="number" name="age" required/>
				<div class="invalid-feedback">
		            Introduce una edad.
		        </div>
			</div>

			<div class="mb-3">
				<label for="province">Provincia</label>
				<select class="form-control" name="province" required/>
					<?php
					foreach ($provinces as $i):
					?>
					<option value="<?php echo $i->id_provincia;?>"><?php echo $i->nombre_provincia; endforeach?></option>
				</select>
				<div class="invalid-feedback">
		            Introduce una provincia.
		        </div>
			</div>

			<div class="mb-3">
				<label for="address">Dirección</label>
				<input class="form-control" type="text" name="address" required/>
				<div class="invalid-feedback">
		            Introduce una dirección.
		        </div>
		    </div>

			<div class="mb-3">
				<label for="phoneNumber">Teléfono</label>
				<input class="form-control" type="number" name="phoneNumber" required/>
				<div class="invalid-feedback">
		            Introduce un número de teléfono.
		        </div>
		    </div>

		    <div class="mb-3">
				<label for="role">Privilegios</label>
				<select class="form-control" type="number" name="role" required/>
					<option value=""></option>
					<?php
					foreach ($roles as $i):
					?>
					<option value="<?php echo $i->id;?>"><?php echo $i->nivel; endforeach?></option>
				</select>
		    </div>

			<button class="btn btn-primary btn-lg btn-block" type="submit" name="enviar" id="enviar">Enviar</button>

			<input type="text" name="date" value="<?php //date; ?>" hidden />
		</form>
	</main>

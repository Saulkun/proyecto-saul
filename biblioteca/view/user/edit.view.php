<main class="form-signin pt-5" id="formRegister">
	<div class="container">
		<h2 class="row pt-5 text-center">Editar Usuario</h2>
		<form action="?c=user&a=edit" method="post" name="formulario">

			<input type="hidden" name="userId" value="<?php echo $user->user_id; ?>" />

			<div class="mb-2">Nombre: <?php echo $user->nombre_usuario ." ". $user->apellidos; ?></div>

			<div class="mb-2">ID usuario: <?php echo $user->user_id; ?></div>

			<div class="form-group">
				Edad: 
				<input class="form-control" name="age" type="text" value="<?php echo $user->edad; ?>" />
			</div>

			<div class="form-group">
				Dirección: 
				<input class="form-control" name="address" type="text" value="<?php echo $user->direccion; ?>" />
			</div>

			<div class="form-group">
				Provincia:
				<select class="form-control" name="province" required/>
					<?php
					foreach ($provinces as $i):
					?>
					<option value="<?php echo $i->id_provincia;?>"><?php echo $i->nombre_provincia; endforeach?></option>
				</select>
			</div>

			<div class="form-group">
				Teléfono: 
				<input class="form-control" name="phoneNumber" type="text" value="<?php echo $user->telefono; ?>" />
			</div>

			<div>
				<input type="submit" name="enviar" value="Editar" />
			</div>

		</form>
	</div>
</main>
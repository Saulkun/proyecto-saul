 <div class="container pt-5" role="main">

	<div class="card row mx-auto mt-5 p-2">

		<div class="col">
			<h4 class="mb-3 ml-5"><?php echo $user->nombre_usuario ." ". $user->apellidos; ?></h4>

			<div class="mb-1 ml-5">ID usuario: <?php echo $user->user_id; ?></div>

			<div class="mb-1 ml-5">Nivel: <?php echo $user->nivel;  ?></div>

			<div class="mb-1 ml-5">Estado: <?php echo $user->estado; ?></div>

			<?php

			if ($_SESSION['role'] == Settings::ROLE_ADMIN ||
 			$_SESSION['id'] == $_REQUEST['userId']) {?>	


			<div class="mb-1 ml-5">DNI: <?php echo $user->dni; ?></div>

			<div class="mb-1 ml-5">Edad: <?php echo $user->edad; ?></div>

			<div class="mb-1 ml-5">Dirección: <?php echo $user->direccion; ?></div>

			<div class="mb-1 ml-5">Provincia: <?php echo $user->nombre_provincia; ?></div>

			<div class="mb-1 ml-5">Telefono: <?php echo $user->telefono; ?></div>


	</div>
			<?php
			} ?>

<?php
if (isset($_SESSION['id'])):
	if (in_array($_SESSION['role'], Settings::ROLE_MANAGEMENT)):?>
		<hr/>
		<div class="row">

			<div class="col text-center"><a href="?c=reserve&a=myReserves&userId=<?php echo $user->user_id; ?>"><button class="btn-lg btn-primary" role="button">Alquileres</button></a></div>

		<?php
		if ($_SESSION['role'] == Settings::ROLE_ADMIN):?>
			
			<div class="col text-center"><a href="?c=user&a=crud&userId=<?php echo $user->user_id; ?>"><button class="btn-lg btn-warning" role="button">Editar</button></a></div>

			<?php
			if ($user->estado_usuario == Settings::USER_ENABLED):?>
			<div class="col text-center"><a href="?c=user&a=disable&userId=<?php echo $user->user_id; ?>"><button class="btn-lg btn-danger" role="button">Deshabilitar</button></a></div>

			<?php
			endif;

			if ($user->estado_usuario == Settings::USER_DISABLED):?>
			<div class="col text-center"><a href="?c=user&a=enable&userId=<?php echo $user->user_id; ?>"><button class="btn-lg btn-success" role="button">Habilitar</button></a></div>

			<?php 
			endif;
		endif;
	?>
		</div>
	<?php
	endif;
endif;
?>
</div>


<!doctype html>
<html lang="es">
<head>
  <title>Biblioteca Online</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>

	<main class="form-signin" id="formRegister">
		<form class="needs-validation" id="data" name="data" action="?c=user&a=register" method="POST" >
			<div class="align-self-center"><a href="index.php"><img class="mb-4" src="assets/logo.png" alt="logo" id="logoRegister" width="72" height="72"></a></div>
        	<h1 class="h3 mb-4 fw-normal text-center">Nuevo usuario</h1>

			<div class="row">

				<div class="col-md-6 mb-3">
					<label for="name">Nombre</label><input class="form-control" type="text" name="name" required/>
					<div class="invalid-feedback">
		                Introduce un nombre válido.
		            </div>
		        </div>

		        <div class="col-md-6 mb-3">
					<label for="lastName">Apellidos</label>
					<input class="form-control" type="text" name="lastName" required/>
					<div class="invalid-feedback">
		                Introduce unos apellidos válidos.
		            </div>
				</div>
			</div>

			<div class="mb-3">
				<label for="dni">DNI</label>
				<input class="form-control" type="text" name="dni" />
				<div class="invalid-feedback">
		            Introduce un dni válido.
		        </div>
			</div>

			<div class="mb-3">
				<label for="password">Contraseña</label>
				<input class="form-control" type="password" name="password" required/>
				<div class="invalid-feedback">
		            Introduce una contraseña válida.
		        </div>
			</div>

			<div class="mb-3">
				<label for="age">Edad</label>
				<input class="form-control" type="number" name="age" required/>
				<div class="invalid-feedback">
		            Introduce una edad.
		        </div>
			</div>

			<div class="mb-3">
				<label for="province">Provincia</label>
				<select class="form-control" name="province" required/>
					<?php
					foreach ($provinces as $i):
					?>
					<option value="<?php echo $i->id_provincia;?>"><?php echo $i->nombre_provincia; endforeach?></option>
				</select>
				<div class="invalid-feedback">
		            Introduce una provincia.
		        </div>
			</div>

			<div class="mb-3">
				<label for="address">Dirección</label>
				<input class="form-control" type="text" name="address" required/>
				<div class="invalid-feedback">
		            Introduce una dirección.
		        </div>
		    </div>

			<div class="mb-3">
				<label for="phoneNumber">Teléfono</label>
				<input class="form-control" type="number" name="phoneNumber" required/>
				<div class="invalid-feedback">
		            Introduce un número de teléfono.
		        </div>
		    </div>

			<button class="btn btn-primary btn-lg btn-block" type="submit" name="enviar" id="enviar">Enviar</button>
		</form>
	</main>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>

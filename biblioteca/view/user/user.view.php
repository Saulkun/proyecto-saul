<div class="container pt-5">
	<h1 class="pt-5">Usuarios</h1>

	<form class="form-inline my-2 my-lg-0 mr-5 " method="post" action="?c=user&a=search">
          	<input class="form-control mr-sm-2" type="text" placeholder="Buscar usuario" aria-label="Search" name="search">
          	<button class="btn btn-outline-success my-2 my-sm-0" type="submit">
          		<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
					<path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
				</svg>
			</button>
    </form>
	<?php
	if (isset($users)):
		foreach ($users as $i):?>
			<div class="row mt-3">
				<div><a href="?c=user&a=profile&userId=<?php echo $i->id; ?>"><?php echo $i->nombre . " " . $i->apellidos; ?></a></div>
				<div>ID: <?php echo $i->id; ?></div>
			</div>
	<?php
		endforeach;
	endif;
	?>
</div>
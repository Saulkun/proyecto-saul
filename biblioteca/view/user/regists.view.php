<div class="container pt-5">
	<h2 class="pt-5">Peticiones de registro</h2>

	<h5 class="mb-5">Comprobar los datos del usuario antes de aceptarlo</h5>

	<?php
	foreach ($list as $i):?>
		<div class="mb-5">
			<form id="addUserForm" name="addUserForm" action="?c=user&a=accept" method="POST">
				<div>DNI: <?php echo $i->dni; ?></div>
				<div>Nombre: <?php echo $i->nombre; ?></div>
				<div>Apellidos: <?php echo $i->apellidos; ?></div>
				<div>Edad: <?php echo $i->edad; ?></div>
				<div>Dirección: <?php echo $i->direccion; ?></div>
				<div>Teléfono: <?php echo $i->telefono; ?></div>
				<div>Fecha de registro: <?php echo $i->fecha_registro ?></div>
				<input type="text" name="userId" value="<?php echo $i->id; ?>" hidden/>
				
				<input class="mr-4 btn btn-primary" type="submit" id="confirmUser" name="confirmUser" value="Aceptar" />
				<a class="btn btn-warning" value="Rechazar" href="?c=user&a=disable&userId=<?php echo $i->id; ?>">Rechazar</a>
			</form>
		</div>

	<?php
	endforeach;
	?>
</div>
<div class="container pt-5">
	<h1 class="pt-5">Reservas</h1>

	<form class="form-inline my-2 my-lg-0 mr-5 " method="post" action="?c=reserve&a=search">
          	<input class="form-control mr-sm-2" type="text" placeholder="Buscar reserva" aria-label="Search" name="search">
          	<button class="btn btn-outline-success my-2 my-sm-0" type="submit">
          		<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
					<path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
				</svg>
			</button>
    </form>
	<?php
	if (isset($reserves)):
		foreach ($reserves as $i):?>
			<form action="?c=reserve&a=start" method="POST">
				<div class="row mt-3">
					<div><a href="?c=user&a=profile&userId=<?php echo $i->id; ?>">
						<?php echo $i->nombre;?></a></div>
					<div><a href="?c=book&a=profile&bookId=<?php echo $i->codigo_libro; ?>"><?php echo $i->titulo; ?></a></div>
					<div class="row">
						<div class="col">ID usuario</div>
						<div class="col">ID alquiler</div>
						<div class="col">Fecha de reserva</div>
						<div class="col">Fecha fin reserva</div>
					</div>
					<div class="row">
						<div class="col"><?php echo $i->id; ?></div>
						<div class="col"><?php echo $i->codigo_alquiler; ?></div>
						<div class="col"><?php echo $i->fecha_reserva; ?></div>
						<div class="col"><?php echo $i->fecha_limite; ?></div>
					</div>
				</div>

				<div class="mt-3">
					<input type="text" name="bookId" value="<?php echo $i->codigo_libro; ?>" hidden/>
					<input type="text" name="reserveId" value="<?php echo $i->codigo_alquiler; ?>" hidden/>

					<input class="mr-4 btn btn-primary" type="submit" value="Aceptar" />

					<a class="btn btn-danger" href="?c=reserve&a=end&reserveId=<?php echo $i->codigo_alquiler; ?>&bookId=<?php echo $i->codigo_libro; ?>">Cancelar</a>

				</div>
			</form>					
	<?php
		endforeach;
	endif;
	?>
</div>
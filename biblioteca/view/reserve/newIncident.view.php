<div class="container pt-5">
			
	<h2 class="pt-5 text-center">Nueva incidencia</h2>

	<form class="form my-2 my-lg-0 mr-5 " method="post" action="?c=reserve&a=sendIncident">
		<div class="mb-2">ID reserva: <?php echo $_REQUEST['reserveId'];?></div>
		<input type="text" value="<?php echo $_REQUEST['reserveId'];?>" name="reserveId" hidden>

		<div class="mb-2">ID libro: <?php echo $_REQUEST['bookId'];?></div>
		<input type="text" value="<?php echo $_REQUEST['bookId'];?>" name="bookId" hidden>

		<label for="incident">Tipo de incidencia:</label>
		<select class="form-control mb-3" name="incident" id="incident" required/>
			<option value="">Selecciona una opción</option>
			<?php
			foreach ($incidents as $i):?>
				<option value="<?php echo $i->id_incidencia;?>"><?php echo $i->incidencia; endforeach?></option>
		</select>

		<label for="incident">Describe lo ocurrido:</label>
		<textarea class="form-control mb-5" name="details" required></textarea>

		<input type="submit" class="btn btn-primary" value="Enviar" />
	</form>
</div>
	
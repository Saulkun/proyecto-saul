<div class="container pt-5 h-100" role="main" id="index" name="index">
	<h2 class="pt-5 text-center" id="catalogo">Mis Reservas</h2>

	<?php

	foreach ($list as $i):?>
		<div class="row mx-auto mb-3 text-left">
			<div class="col-12 col-sm-4 col-md-7">
				<div class="row mt-3">
				<div><a href="?c=book&a=profile&bookId=<?php echo $i->codigo_libro; ?>"><?php echo $i->titulo; ?></a></div>
				<div>Inicio de reserva: <?php echo $i->fecha_reserva; ?></div>
				<div>Fecha fin reserva: <?php echo $i->fecha_limite; ?></div>
				<div>ID libro: <?php echo $i->codigo_libro; ?></div>
			</div>
			</div>

		</div>

		<hr/>
	<?php
	endforeach;
	?>

	<h2 class="pt-5 text-center" id="catalogo">Mis alquileres</h2>

	<?php

	foreach ($loan as $i):?>
		<div class="row mx-auto mb-3 text-left">
			<div class="col-12 col-sm-4 col-md-7">
				<div class="row mt-3">
				<div><a href="?c=book&a=profile&bookId=<?php echo $i->codigo_libro; ?>"><?php echo $i->titulo; ?></a></div>
				<div>Inicio de reserva: <?php echo $i->fecha_reserva; ?></div>
				<div>Fecha límite de reserva: <?php echo $i->fecha_limite; ?></div>
				<div>Inicio de alquiler: <?php echo $i->fecha_inicio; ?></div>
				<div>Fecha límite de alquiler: <?php echo $i->fecha_final; ?></div>
				<div>ID libro: <?php echo $i->codigo_libro; ?></div>
			</div>
			</div>

		</div>

		<hr/>
	<?php
	endforeach;
	?>
</div>
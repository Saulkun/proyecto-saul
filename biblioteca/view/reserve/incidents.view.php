<div class="container pt-5">
			
	<h1 class="pt-5">Incidencias</h1>
	
	<form class="form-inline my-2 my-lg-0 mr-5 " method="post" action="?c=reserve&a=incidentSearch">
          	<input class="form-control mr-sm-2" type="text" placeholder="Buscar incidencia" aria-label="Search" name="search">
          	<button class="btn btn-outline-success my-2 my-sm-0" type="submit">
          		<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
					<path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
				</svg>
			</button>
    </form>

	<?php
	if (isset($incidents)):
		foreach ($incidents as $i):?>
		<div class="row">
			<div class="col">ID usuario</div>
			<div class="col">ID alquiler</div>
			<div class="col">ID libro</div>
			<div class="col">Fecha de reserva</div>
			<div class="col">Inicio de alquiler</div>
			<div class="col">Fecha devolución</div>
			<div class="col">Tipo de incidencia</div>
			<div class="col">Descripción</div>
		</div>

		<div class="row mb-3">
			<div class="col"><a href="?c=user&a=profile&userId=<?php echo $i->id_usuario; ?>"><?php echo $i->id_usuario; ?></a></div>
			<div class="col"><?php echo $i->codigo_alquiler; ?></div>
			<div class="col"><a href="?c=book&a=profile&bookId=<?php echo $i->codigo_libro; ?>"><?php echo $i->codigo_libro; ?></a></div>
			<div class="col"><?php echo $i->fecha_reserva; ?></div>
			<div class="col"><?php echo $i->fecha_inicio; ?></div>
			<div class="col"><?php echo $i->fecha_devolucion; ?></div>
			<div class="col"><?php echo $i->incidencia; ?></div>
			<div class="col"><?php echo $i->descripcion_incidencia; ?></div>
		</div>

<?php
		endforeach;
	endif;
?>
</div>
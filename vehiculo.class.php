<?php

class Vehiculo {
	private $tipoVehiculo;
	private $kilometros;
	protected $precio;

	public function __construct (string $tipoVehiculo, int $kilometros) {
		$this->tipoVehiculo = $tipoVehiculo;
		$this->kilometros = $kilometros;
		$this->precio = 0;
	}

	protected function calcularPrecio() {
		if ($this->tipoVehiculo == "bicicleta") {
            $this->precio = $this->kilometros * 10;
            return $this;
		}

		$this->precio = $this->kilometros * 30;
		return $this;
	}

	public function precioVehiculo() {
		if ($this->tipoVehiculo != "camion") {
			$this->calcularPrecio();
		}
		
		echo "El precio para su {$this->tipoVehiculo} es de {$this->precio} €.";
	}
}

class Camion extends Vehiculo {
	private $carga;

	public function __construct (string $tipoVehiculo, int $kilometros, int $carga){
		$this->carga = $carga;
		parent::__construct($tipoVehiculo, $kilometros);
	}

	private function anadirCarga() {
		$this->calcularPrecio();
		$this->precio += $this->carga * 25;
		return $this;
	}

	public function precioVehiculo() {
		$this->anadirCarga();
		parent::precioVehiculo();
	}
}

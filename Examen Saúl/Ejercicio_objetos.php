<?php
/*
Hay dos opciones: 
	1- Definir la clase Vehículo como abstract entera (con sus atributos y métodos).
	2- No definirla como abstract.
Se ha optado por la primera opción, ya que vehículo no es un objeto real, la clase tiene más sentido como un modelado general de un vehículo cualquiera.
*/
abstract class Vehículo { // La primera letra del nombre de la clase debe ser en mayusculas " Vehiculo "
	abstract protected $color; // Los atributos deben definirse como protected para que luegos los herede la clase Coche. Faltaba un ; al final de la frase
	abstract protected $matricula;

	abstract public function __construct(string $color, string $matricula) { // Matrícula tiene letras, debe pedirse como string
		$this->color = $color;
		// Se elimina la asignación del atributo bastidor, la clase no tiene ese atributo.
		$this->matricula = $matricula; // Faltaba un ; al final de la frase
		// Para acceder al atributo de un objeto no hay que poner el simbolo " $ ". Ejemplo: $this->$color debería ser $this->color
	}

	abstract protected function setMatricula(string $matricula) {
		$this->matricula = $matricula;
		return $this; //Faltaba el return para que nos devuelva la matrícula
	}

	abstract protected function setColor(string $color) { // El método setColor() está definido como private, debería ser protected para que la clase Coche pueda utilizar ese método. La variable $color que se le pasa a la función setColor no debería igualarse a null, si no cada vez que usemos este método el vehículo pasaría a no tener color.
		$this->color = $color; // $this->$color debería ser $this->color
		return $this;
	}

	/* El método getPuertas debe eliminarse porque la clase vehículo no tiene puertas que devolver. 
	   Se definirá en la clase Coche.
	*/

	abstract public function getMatricula() { // Se añade la función getMatricula para poder obtener la matricula del vehiculo, ya que no podemos acceder directamente al atributo
		return $this->matricula;
	}

	abstract public function getColor() { // Se añade la función getColor para poder obtener el color del vehiculo, ya que no podemos acceder directamente al atributo
		return $this->color;
	}
}

class Coche extends Vehiculo { // La clase coche hereda de Vehiculo, no puede implementar porque Vehículos no es una interfaz. Debe ponerse " extends "
	private $numPuertas; // Los atributos deben ponerse como protected o private en lugar de public por seguridad

	public function __construct(string $color, string $matricula, int $numPuertas) { /* $color y $matricula contienen letras, se deben pedir como strings.
	Faltaba _ en el constructor.
	 */
		$this->numPuertas = $numPuertas; //Sobra $ y el nombre del atributo estaba mal escrito
		//Se elimina $this->color = $color; porque ya estaba definido en la clase Vehiculo. Le Faltaba ; y sobraba $
		parent::__construct($matricula, $color);
	}

	public function getPuertas(int $puertas) { //Faltaba int a la hora de pasar la variable $puertas
		$this->puertas = $puertas; //Sobraba $
		return $this;
	} 

	// Se eliminar la function getMatricula porque ya estaba definida en la clase Vehiculo
}

$miPrimerCoche = new Coche ('rojo', '1833CIF', 5); //Se sustituye vehículo por coche porque la clase vehículo es abstracta y no puede ser instanciada, además el nombre de la variable indica que es un coche y debe tener puertas;
$miSegundoCoche = new Coche ('verde', '1833CIF', 5);

echo "Mi primer coche tiene {$miPrimerCoche->getPuertas()} puertas"; //Se debe llamar al objeto con el nombre de la variable en la que fue instanciado. $miPrimerCoche en lugar de $Vehiculo.
echo "Mi segundo coche tiene {$miSegundoCoche->getPuertas()} puertas";